<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('/index2', function () {
//    dd(config('mail'));
})->name('index2');

Route::get('logout', function(){
        Auth::logout();
        return redirect()->route('index');
    })->name('voyager.logoutG');

Route::post("request/store", "RequestController@store")->name('request.store');

Route::group(['prefix' => 'usuario'], function () {

    Route::group(['middleware' => ['auth', 'verified']], function () {    
    Route::get("mis-datos", "LoansController@index")->name('user.data');
    Route::post("mis-datos/store", "LoansController@update")->name('user.dataUpdate');
    Route::get("mis-datos/fotodni", "LoansController@dni")->name('fotodni'); 
    
    Route::get("mis-prestamos", "LoansController@loans")->name('user.loans');
    Route::get("mis-prestamos/{id}", "LoansController@loan")->name('user.loan');
    
    Route::post("request/store", "RequestController@storeNew")->name('user.store');
    
    Route::get("contacto", "LoansController@contact")->name('user.contact');
    Route::post("contacto", "LoansController@contactSave")->name('user.contactSave');
    });    
    Route::get("pay/{id}", "LoansController@pay")->name('user.loanpay');
    
});

Route::get("preguntas-frecuentes", "LoansController@faq")->name('faq');

Route::get('efectivo-ipn', 'LoansController@ipn')->name('pay.ipn.get');
Route::post('efectivo-ipn', 'LoansController@ipn')->name('pay.ipn.post');

Route::get('mercado/callback', 'LoansController@callback')->name('mercado.callback');
Route::post('mercado/callback', 'LoansController@callback')->name('mercado.callback.post');

Route::get('validemail', 'LoansController@validEmail')->name('valid.email');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    
    Route::get("mis-datos/fotodni", "LoansController@dni")->name('admin.fotodni'); 
});


Auth::routes(['verify' => true]);

Route::get('/register', function(){
    return redirect()->route('index');
})->name('register');

Route::get('/ref', 'ReferidosController@index')->name('referido');
Route::get('/tk', 'LoansController@decidir');
Route::post('/tk', 'LoansController@decidir')->name('decidir');



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use App\Decidir;
use Session;

class LoansController extends Controller
{

    public function index(){

        if(auth()->user()){
            $user = \App\User::find(auth()->user()->id);
            $userId = auth()->user()->id;

            $requestLoan = \App\Request::where('user_id', $userId)->first();
        }else{
            $requestLoan = false;
            $user = false;
        }

        $menuUser = true;

        return view('user.misdatos', compact('requestLoan', 'user', 'menuUser'));
    }

    public function update(Request $request){

        $user = \App\User::find(auth()->user()->id);

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->bank_id = $request->banco;
        $user->type_account_id = $request->tipoCuenta;
        $user->cbu = $request->cbu;
        $user->cuit = $request->cuit;

        $actions[] = 'Modificó datos personales Actuales =>'."-(Nombre: {$user->name}, "
        . "Apellido: {$user->lastname}, Email: {$user->email}, Domicilio: {$user->address},"
        . "Banco: {$user->bank_id}, Tipo cuenta: {$user->type_account_id}, CBU: {$user->cbu}, CUIT: {$user->cuit})\n"
         . " Nuevos => (Nombre: {$request->name}, "
        . "Apellido: {$request->lastname}, Email: {$request->email}, Domicilio: {$request->address},"
        . "Banco: {$request->banco}, Tipo cuenta: {$request->tipoCuenta}, CBU: {$request->cbu}, CUIT: {$request->cuit})";

        $saveUser = $user->save();

        if(!$saveUser){
            return redirect()->route('user.data')->with([
                'message'    => 'No fue posible actualizar los datos, por favor intente de nuevo',
                'alert-type' => 'warning'
            ]);
        }

        if($saveUser && strlen($request->password)>0 && $request->password == $request->confirm){
            $user->password = Hash::make($request->password);
            $user->save();
            $actions[] = 'Modificó su contraseña';
        }

        if($saveUser && $request->file('dni1')){

            $actions[] = 'Modificó Frente de DN1';

            Storage::disk('private')->delete('dni/dni-frente-'.$user->id.'.png');
            Storage::disk('private')->putFileAs('dni', $request->file('dni1'), 'dni-frente-'.$user->id.'.png');
        }
        if($saveUser && $request->file('dni2')){

            $actions[] = 'Modificó Dorso de DN1';

            Storage::disk('private')->delete('dni/dni-dorso-'.$user->id.'.png');
            Storage::disk('private')->putFileAs('dni', $request->file('dni2'), 'dni-dorso-'.$user->id.'.png');
        }

        \App\Log::actions($actions);

        return redirect()->route('user.data')->with([
            'message'    => 'Datos actualizados con éxito',
            'alert-type' => 'success'
            ]);
    }

    public function dni(Request $request){

        if(!auth()->user()){
            return;
        }

        $name = ($request->d == "1") ? 'dni-frente-' : 'dni-dorso-';
        $mime = Storage::disk('private')->mimeType('dni/'.$name.$request->id.'.png');
        if(in_array($mime, ['image/pjpeg', 'image/jpeg', 'image/jpg'])){
            $name2 = $request->id.'.jpg';
        }else{
            $name2 = $request->id.'.png';
        }

        return Storage::disk('private')->download('dni/'.$name.$request->id.'.png', $name2);
    }

    public function loans(Request $request){

        $user = \App\User::find(auth()->user()->id);
        $userId = auth()->user()->id;

        $loans = \App\Request::where('user_id', $userId)->orderBy('created_at', 'desc')->paginate(4);

        $menuUser = true;
       $id=0;
        return view('user.misprestamos', compact('loans', 'user', 'menuUser', 'id'));
    }

    public function loan(Request $request, $id){

        $user = \App\User::find(auth()->user()->id);
        $userId = auth()->user()->id;

        $loan = \App\Request::find($id);

        $menuUser = true;


        return view('user.misprestamos', compact('loan', 'user', 'menuUser'));
    }

    public function pay(Request $request){

//        $urlQuery = parse_url(urldecode(html_entity_decode($request->back_url)), PHP_URL_QUERY);
//        $queries = explode("&", $urlQuery);
//        $status = explode("=", $queries[0]);
//        $request->status = $status[1];
        if(!isset($request->status)){
            return redirect()->route("user.loans");
        }


       // ?preference_id=153752169-bdc9d2a3-60ee-44bf-8645-7be4e7547145
       // &external_reference=
       // &back_url=http%3A%2F%2F123efectivo.walksoft.com.co%2Fusuario%2Fpay%2F24%3Fstatus%3Dok%26collection_id%3D25770031%26collection_status%3Dapproved%26external_reference%3Dnull%26payment_type%3Dcredit_card%26merchant_order_id%3D1331420362%26preference_id%3D153752169-bdc9d2a3-60ee-44bf-8645-7be4e7547145%26site_id%3DMCO%26processing_mode%3Daggregator%26merchant_account_id%3Dnull
       // &payment_id=25770031
       // &payment_status=approved
       // &payment_status_detail=accredited
       // &merchant_order_id=1331420362
       // &processing_mode=aggregator&merchant_account_id=
        $loan = \App\Request::find($request->id);
        $dMessage = [];
        switch ($request->status){
            case'ok':
                $dMessage = [
                    'message'    => 'Mercado pago configurado con éxito',
                    'alert-type' => 'success'
                    ];

                $mercado = \App\Mercadopago::where('request_id', $loan->id)->first();
                $mercado->status = $request->status;
                $mercado->pay_id = $request->payment_id;
                $mercado->save();

                $loan->status = 2;

                $loan->save();

                $actions[] = 'Pagado mercadopago: '.$mercado->preference;

                $cRef = \App\Refer::where('user_id', $loan->user_id)->count();
                if($cRef>0){

                    $cRef = \App\Refer::where('user_id', $loan->user_id)->first();

                    $ref = new \App\Refer();
                    $ref->request_id = $loan->id;
                    $ref->referer = $cRef->referer;
                    $ref->status = 3;
                    $ref->track = $cRef->track;
                    $ref->user_id = $loan->user_id;
                    $ref->pay = 1;
                    $ref->save();
                }


                break;
            case'fail':
                $dMessage = [
                    'message'    => 'No se configuró Mercado pago con éxito',
                    'alert-type' => 'warning'
                    ];
                if($loan){
                    $mercado = \App\Mercadopago::where('request_id', $loan->id)->first();
                    $mercado->status = $request->status;
                    $mercado->pay_id = $request->payment_id;
                    $mercado->save();
                }
                $actions[] = 'Falló mercadopago';
                break;
            case'pending':
                $dMessage = [
                    'message'    => 'En espera de confirmación de Mercado pago ',
                    'alert-type' => 'info'
                    ];
                    if($loan){
                        $mercado = \App\Mercadopago::where('request_id', $loan->id)->first();
                        $mercado->status = $request->status;
                        $mercado->pay_id = $request->payment_id;
                        $mercado->save();
                    }
                $actions[] = 'Pendiente mercadopago';
                break;
            default:
                 if($loan){
                    $mercado = \App\Mercadopago::where('request_id', $loan->id)->first();
                    $mercado->status = $request->status;
                    $mercado->pay_id = $request->payment_id;
                    $mercado->save();
                }
        }

        \App\Log::actions($actions);

        return redirect()->route('user.loan', ['id' => $loan->id])->with($dMessage);
    }

    public function contact(Request $request){

        $menuUser = true;

        return view('user.contacto', compact('menuUser'));
    }

    public function contactSave(Request $request){

        $contact = new \App\Contact();

        $contact->subject = $request->subject;
        $contact->mercadopago = $request->mercadopago;
        $contact->message = $request->message;
        $contact->user_id = auth()->user()->id;

        $contact->save();

        \App\Log::actions(['Contacto: '.$request->subject]);

        return redirect()->route('user.contactSave')->with([
            'message'    => 'Se ha registrado su solicitud, nos comunicaremos con vos a la brevedad.',
            'alert-type' => 'success'
        ]);
    }

    public function faq(){

         $menuUser = (auth()->user() ? true : false);

         $faqs = \App\Faq::where('estado', 1)->get();

        return view('user.faq', compact('faqs', 'menuUser'));
    }

    public function ipn(Request $request){


      //die("Stop");
        if(!isset($request->topic)){
            return response()->noContent(Response::HTTP_CREATED);
        }

        $ipn = new \App\Ipn();

        $ipn->topic = $request->topic;
        $ipn->id_m = $request->id;
        $ipn->data = serialize($request->all());

        $ipn->save();
        /*
        $topic = $request->topic;
        $id = $request->id;

        $mercado = \App\Mercadopago::where('request_id', $id)->first();
        $mercado->status = 'ok'.$topic;

        $mercado->save();

        MercadoPago\SDK::setAccessToken(ACCESS_TOKEN);
$info = json_decode($this->input->raw_input_stream);
$payment = new MercadoPago\Payment();
$or_collection_id = $info->data->id;
$find_info = MercadoPago\Payment::find_by_id($or_collection_id);
$or_number = $find_info->external_reference;

var_dump($find_info);*/

        return response()->noContent(Response::HTTP_CREATED);

    }

    public function validEmail(Request $request){

        $count = \App\User::where('email', $request->email)->count();

        echo $count > 0? 'false' : 'true';
    }

    public function callback(Request $request){

        $cb = new \App\Callback();

        $cb->data = serialize($request->all());

        $cb->save();

        echo "Ok";

    }

    public function decidir(Request $request){
        
        $loan = \App\Request::findOrfail($request->loan);
        
        $keys_data = array('public_key' => Decidir::$public_key, 'private_key' => Decidir::$private_key);

        $ambient = Decidir::$ambient;//valores posibles: "test" o "prod"

        $fields = [
            'card_number' => $this->decrypt($request->card_number, $request->_token.$request->loan, false),
            'card_expiration_month' => $this->decrypt($request->card_expiration_month, $request->_token.$request->loan),
            'card_expiration_year' => $this->decrypt($request->card_expiration_year, $request->_token.$request->loan),
            'security_code' => $this->decrypt($request->security_code, $request->_token.$request->loan),
            'card_holder_name' => $this->decrypt($request->card_holder_name, $request->_token.$request->loan),
            'card_holder_identification' => [
                'type' => 'dni',
                'number' => $this->decrypt($request->dni, $request->_token.$request->loan)
            ]
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => Decidir::$url."/api/v2/tokens",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($fields),
          CURLOPT_HTTPHEADER => array(
            "apikey: ".Decidir::$public_key,
            "cache-control: no-cache",
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err ) {
            return "Error 1";
        }

        $tk = json_decode($response);
        
        if(isset($tk->error_type)){
            return "Error 2";
        }

        $id = $tk->id;
        $bin = substr($fields['card_number'], 0, 6);

        $connector = new \Decidir\Connector($keys_data, $ambient);
        $data = array(
            "site_transaction_id" => "1204200". uniqid(),
            "token" => $id,
            "payment_method_id" => 1,
            "bin" => $bin,
            "amount" => $loan->value,
            "currency" => "ARS",
            "installments" => $loan->quotes,
            "description" => "Prestamo ".$loan->id." 123efectivo d",
            "establishment_name" => "123efectivo.com",
            "payment_type" => "single",
            "sub_payments" => array()
          );

        try{
            $response = $connector->payment()->ExecutePayment($data);
            $dMessage = [];
            
            $r = (array)$response; 
            $firstKey = array_key_first($r);
            $response = $r[$firstKey];
            switch ($response['status']){
                case'approved':
                    $dMessage = [
                        'message'    => 'Pago realizado con éxito',
                        'alert-type' => 'success'
                        ];

                    $mercado = new \App\Decidir();
                    $mercado->request_id = $loan->id;
                    $mercado->status = 'ok';
                    $mercado->token = $id;
                    $mercado->data = json_encode($response);
                    $mercado->save();

                    $loan->status = 2;

                    $loan->save();

                    $actions[] = 'Pagado Decidir: '.$mercado->preference;

                    $cRef = \App\Refer::where('user_id', $loan->user_id)->count();
                    if($cRef>0){

                        $cRef = \App\Refer::where('user_id', $loan->user_id)->first();

                        $ref = new \App\Refer();
                        $ref->request_id = $loan->id;
                        $ref->referer = $cRef->referer;
                        $ref->status = 3;
                        $ref->track = $cRef->track;
                        $ref->user_id = $loan->user_id;
                        $ref->pay = 1;
                        $ref->save();
                    }


                    break;
                case 'rejected':
                    $dMessage = [
                        'message'    => 'Se rechazo pago',
                        'alert-type' => 'warning'
                        ];
                    if($loan){
                        $mercado = new \App\Decidir();
                        $mercado->request_id = $loan->id;
                        $mercado->status = 'fail';
                        $mercado->token = $id;
                        $mercado->data = json_encode($response);
                        $mercado->save();
                    }
                    $actions[] = 'Rechazo decidir';
                    break;
                case'pre_approved':
                    $dMessage = [
                        'message'    => 'En espera de confirmación de decidir ',
                        'alert-type' => 'info'
                        ];
                        if($loan){
                            $mercado = new \App\Decidir();
                            $mercado->request_id = $loan->id;
                            $mercado->status = 'pending';
                            $mercado->token = $id;
                            $mercado->data = json_encode($response);
                            $mercado->save();
                        }
                    $actions[] = 'Pendiente Decidir';
                    break;
                case'review':
                    $dMessage = [
                        'message'    => 'Transacción que debe ser revisada manualmente ',
                        'alert-type' => 'info'
                        ];
                        if($loan){
                            $mercado = new \App\Decidir();
                            $mercado->request_id = $loan->id;
                            $mercado->status = 'review';
                            $mercado->token = $id;
                            $mercado->data = json_encode($response);
                            $mercado->save();
                        }
                    $actions[] = 'Transacción que debe ser revisada manualmente';
                    break;
                default:
                    
                    $dMessage = [
                        'message'    => 'Respuesta desconocida ',
                        'alert-type' => 'info'
                        ];
                     if($loan){
                        $mercado = new \App\Decidir();
                        $mercado->request_id = $loan->id;
                        $mercado->status = $response['status'];
                        $mercado->token = $id;
                        $mercado->data = json_encode($response);
                        $mercado->save();
                    }
            }

            \App\Log::actions($actions);
        } catch( \Exception $e ) {
            var_dump($e->getData());
        }

        Session::flash('message', $dMessage['message']); 
        Session::flash('alert-type', $dMessage['alert-type']);
        
        return $dMessage['message'];
    }
    
    private function decrypt($data, $key, $space = true){
        $str = base64_decode(\App\AesCtr::decrypt(($data), $key, 256));
        
        if(!$space){
            $str = str_replace(" ", "", $str);
        }
        return $str;
    }
}

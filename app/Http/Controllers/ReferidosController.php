<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Cookie;
use Hash;

class ReferidosController extends Controller
{
   public function index(Request $request){
       
       if(!isset($request->cff)){
            return redirect()->route('index');    
       }
       
       $ref = new \App\Refer();
       $ref->status = 1;
       $ref->referer = $request->cff;
       $ref->track = isset($request->id) ? $request->id : '';
       $ref->data = json_encode($request->all());
       $ref->save();
       
       //session([ 'refer' => $request->cff ]);
       session_start();
       $_SESSION['refer']=$ref->id;
       Session::put('refer', $ref->id);
       
        
       return redirect( str_replace('/public', '', route('index')) )->with([
           'refer' => $ref->id
        ])->withCookie(Cookie::make('referer', $ref->id, 60*24*30));
   }
}

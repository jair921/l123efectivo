<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class RequestController extends Controller
{
    public function store(Request $request){
        
       
        $validatedData = $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'lastname' => 'required',
            'cuit' => 'required',
            'tipoCuenta' => 'required',
            'banco' => 'required',
            'cbu' => 'required',
            'phone' => 'required',
        ]);

        
        $user = new \App\User();
        $user->role_id = 2;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = Hash::make($request->cuit);
        
        $user->cuit = $request->cuit;
        $user->type_account_id = $request->tipoCuenta;
        $user->bank_id = $request->banco;
        $user->cbu = $request->cbu;
        $user->phone = $request->phone;
 
        try{
            $saveUser = $user->save();
        }catch(\Illuminate\Database\QueryException $e){
            $saveUser = false;
        }
        
        if(!$saveUser){
            return redirect()->route('user.data')->with([
                'message'    => 'No fue posible recibir su solicitud.',
                'alert-type' => 'warning'
            ]);
        }
        
        $requestLoan = new \App\Request();
        $requestLoan->user_id = $user->id;
        $requestLoan->value = $request->value;
        $requestLoan->quotes = $request->options;
        $requestLoan->quotes_value = $request->quotes_value;
        
        try{
            $requestSave = $requestLoan->save();
        }catch(\Illuminate\Database\QueryException $e){
             $requestSave = false;
        }
        
        if(!$requestSave){
            return redirect()->route('user.data')->with([
                'message'    => 'No fue posible recibir su solicitud.',
                'alert-type' => 'warning'
            ]);
        }

        if($requestSave){ 
            Storage::disk('private')->putFileAs('dni', $request->file('dni1'), 'dni-frente-'.$user->id.'.png');
            Storage::disk('private')->putFileAs('dni', $request->file('dni2'), 'dni-dorso-'.$user->id.'.png');
            
            Auth::login($user);
            event(new \Illuminate\Auth\Events\Registered($user));
            if(isset($request->ref)){
                $idRef = base64_decode($request->ref);
                $ref = \App\Refer::find($idRef);
                if($ref){
                    $ref->request_id = $requestLoan->id;
                    $ref->user_id = $user->id;
                    $ref->save();
                }
            }
        }
        
        \App\Log::actions(['Nuevo préstamo registrado: '.$requestLoan->id]);

        return redirect()->route('user.data')->with([
            'message'    => 'Solicitud registrada con éxito',
            'alert-type' => 'success'
            ]); 
    }
    
    public function storeNew(Request $request){
  
        $requestLoan = new \App\Request();
        $requestLoan->user_id = auth()->user()->id;
        $requestLoan->value = $request->value;
        $requestLoan->quotes = $request->options;
        $requestLoan->quotes_value = $request->quotes_value;
        
        try{
            $requestSave = $requestLoan->save();
        }catch(\Illuminate\Database\QueryException $e){
             $requestSave = false;
        }
        
        if(!$requestSave){
            return redirect()->route('user.data')->with([
                'message'    => 'No fue posible recibir su solicitud.',
                'alert-type' => 'warning'
            ]);
        }
        
        \App\Log::actions(['Nuevo préstamo registrado: '.$requestLoan->id]);

        return redirect()->route('user.loan', ['id' => $requestLoan->id])->with([
            'message'    => 'Solicitud registrada con éxito',
            'alert-type' => 'success'
            ]); 
    }
}

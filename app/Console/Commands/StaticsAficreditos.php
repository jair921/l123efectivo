<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;

class StaticsAficreditos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aficreditos:statics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de datos a aficreditos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $refs = \App\Refer::where('status', 1)->whereNull('request_id')->get();

       if($refs){
         foreach($refs as $ref){
            try{
                $mount = 0;
                $client = new Client(['base_uri' => 'https://aficreditos.com/']);
                $response = $client->request('POST', 'https://aficreditos.com/statics', [
                  'form_params' => [
                          'referer' => $ref->referer,
                          'track' => $ref->track,
                          'request_id' => $ref->request_id,
                          'pay' => 0,
                          'mount' => $mount,
                          'site' => route('index')
                    ]
               ]);
               $ref->status = 2;
               $ref->save();
           }catch(\GuzzleHttp\Exception\ServerException $e){}
         }
      }
      /////////
       $refs = \App\Refer::where('status', 2)->where('request_id', '>', 0)->get();

       if($refs){
         foreach($refs as $ref){
            try{
                $mount = 0;
                $client = new Client(['base_uri' => 'https://aficreditos.com/']);
                $response = $client->request('POST', 'https://aficreditos.com/statics', [
                  'form_params' => [
                          'referer' => $ref->referer,
                          'track' => $ref->track,
                          'request_id' => $ref->request_id,
                          'pay' => 0,
                          'mount' => $mount,
                          'site' => route('index')
                    ]
               ]);
               $ref->status = 3;
               $ref->save();
           }catch(\GuzzleHttp\Exception\ServerException $e){ }
         }
      }
      /////////
      
      //////
      
      $refs = \App\Refer::where('pay', 1)->get();

       if($refs){
         foreach($refs as $ref){
            try{
                $mount = 0;
                if($ref->request_id){
                   $request = \App\Request::find($ref->request_id); 
                   $mount = $request->value;
                }
                $name = '';
                $userN = \App\User::find($ref->user_id);
                if($userN){
                   $name = $userN->name . ($userN->lastname ? ' '.strtoupper($userN->lastname[0]).'.' : '');
                }
                $client = new Client(['base_uri' => 'https://aficreditos.com/']);
                $response = $client->request('POST', 'https://aficreditos.com/statics', [
                  'form_params' => [
                          'referer' => $ref->referer,
                          'track' => $ref->track,
                          'request_id' => $ref->request_id,
                          'pay' => $ref->pay,
                          'mount' => $mount,
                          'name' => $name,
                          'site' => route('index')
                    ]
               ]);
               
               $ref->pay = 2;
               $ref->save();
           }catch(\GuzzleHttp\Exception\ServerException $e){  echo $e->getMessage();  }
         }
      }
      
      /////////
    }
}

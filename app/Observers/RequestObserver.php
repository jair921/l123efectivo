<?php

namespace App\Observers;

use App\Request;

class RequestObserver
{
    /**
     * Handle the request "created" event.
     *
     * @param  \App\Request  $request
     * @return void
     */
    public function created(Request $request)
    {
        //
    }

    /**
     * Handle the request "updated" event.
     *
     * @param  \App\Request  $request
     * @return void
     */
    public function updated(Request $request)
    {
//        $actions[] = 'Se actualizó solicitud';
//        
//        \App\Log::actions($actions);
    }
    
    public function updating(Request $request){
        
        if($request->isDirty()){
            
            $actions[] = 'Se actualizó solicitud => ( Usuario '.$request->user_id .' - Valor '.$request->value
                    .' - Cuotas '.$request->quotes .' .- Valor Cuota '.$request->quotes_value
                    .' - Estado '.\App\StatusLoans::find($request->status)->name.')'
                    ;
            \App\Log::actions($actions);
        }
        
        
        
    }

    /**
     * Handle the request "deleted" event.
     *
     * @param  \App\Request  $request
     * @return void
     */
    public function deleted(Request $request)
    {
        //
    }

    /**
     * Handle the request "restored" event.
     *
     * @param  \App\Request  $request
     * @return void
     */
    public function restored(Request $request)
    {
        //
    }

    /**
     * Handle the request "force deleted" event.
     *
     * @param  \App\Request  $request
     * @return void
     */
    public function forceDeleted(Request $request)
    {
        //
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Observers\RequestObserver;
use App\Observers\UserObserver;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Request::observe(RequestObserver::class);
        User::observe(UserObserver::class);
        \TCG\Voyager\Models\User::observe(\App\Observers\UserVObserver::class);
    }
}

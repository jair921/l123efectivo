<?php

namespace App;

class TNA {
    
    private $tna = 55;
    
    public function getTna(){
        return $this->tna;
    }
    
    public function addTna($valor){
        return (($valor*($this->tna/100)) + $valor);
    }
}
<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class LoansDayValue extends \TCG\Voyager\Widgets\BaseDimmer {

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run() {

        $loans = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))
                ->sum('value');
        
        $loansSolictadosV = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))
                ->where('status', 1)
                ->sum('value');
        
        $loansPagadosV = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))
                ->where('status', 2)
                ->sum('value');
        $loansAcreditadosV = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))
                ->where('status', 3)
                ->sum('value');
        
        return view('voyager.widgets.loansDay', array_merge($this->config, [
            'icon' => 'voyager-credit-cards',
            'title' => "$ ".number_format($loans, 0)." en  Pr&eacute;stamos solicitados hoy",
            'text' => "$".number_format($loansSolictadosV, 0)." Solicitados - $".number_format($loansPagadosV)." Pagados - $". number_format($loansAcreditadosV, 0)." Acreditados ",
            'button' => [
                'text' => 'Ver ' ,
                'link' => route('voyager.solicitudes.index', ['created_at'=>date('Y-m-d')]),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed() {
        
        return true;
    }

}

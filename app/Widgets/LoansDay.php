<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class LoansDay extends \TCG\Voyager\Widgets\BaseDimmer {

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run() {

        $loans = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))->count();
        $loansSolictados = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))
                ->where('status', 1)
                ->count();
        $loansPagados = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))
                ->where('status', 2)
                ->count();
        $loansAcreditados = \App\Request::where('updated_at', '>=', date('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', date('Y-m-d 23:59:59'))
                ->where('status', 3)
                ->count();
 
        
        return view('voyager.widgets.loansDay', array_merge($this->config, [
            'icon' => 'voyager-file-text',
            'title' => "$loans Pr&eacute;stamos solicitados hoy",
            'text' => "$loansSolictados Solicitados - $loansPagados Pagados - $loansAcreditados Acreditados",
            'button' => [
                'text' => 'Ver ' ,
                'link' => route('voyager.solicitudes.index', ['created_at'=>date('Y-m-d')]),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed() {
        
        return true;
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Log extends Model
{
    
    public static function actions($actions){
        
        foreach($actions as $action){
            $log = new Log();
            $log->message = $action;
            $log->user = (auth()->user() ? auth()->user()->id : null);
            $log->save();
        }
        
    }
}

@extends('layout.master')

@section('content')
<div class="slider_area" id="solicita-un-prestamo">
    <div class="single_slider  d-flex align-items-center slider_bg_1">
        <div class="">
            <div class="row align-items-center mtop">

                <div class="col-xl-7 col-md-7">
                    <div class="slider_text ">
                        <h3>Solicita tu pr&eacute;stamo online, sin vueltas,</h3>
                        <h3>acreditaci&oacute;n al instante en tu cuenta bancaria,</h3>
                        <h3>y al m&aacute;s bajo costo del mercado.</h3>
                        <p>¡Solo necesitas tener tarjeta de credito y CBU!</p>
<!--                        <h2>TNA 55%</h2>-->
                    </div>
                </div>

                <div class="col-xl-5 col-md-5">
                    <div class="simulator">
                        <form method="post" action="{{route('request.store')}}" enctype="multipart/form-data" id="loan">
                            @csrf
                            <div id="step1">
                                <p class="text-form1">¿Cuanto Necesit&aacute;s?</p>
                                <div class="row">
                                    <div class="col-xl-1 col-1 d-xl-block"></div>
                                    <div class="range-value" id="rangeV"></div>
                                    <div class="col-12 col-xl-10">
                                        <input id="range" type="range" min="1000" max="70000" step="1000" oninput="calcular()" onchange="calcular()" name="value">
                                    </div>
                                    <div class="col-xl-1 col-1 d-xl-block"></div>
                                </div>   
                                <div class="row" style="margin-top: -22px;">   
                                    <div class="col-md-1 col-1"></div>
                                    <div class="col-2">
                                        <p class="text-form4 n-mt " style="text-align:left;">$1.000</p>
                                    </div>
                                    <div class="col-6"></div>
                                    <div class="col-2">
                                        <p class="text-form4 n-mt" style="text-align:right;">$70.000</p>
                                    </div>
                                    <div class="col-md-1 col-1"></div>
                                </div> 
                                <div class="row" style="margin-top:-20px !important;">  
                                <div class="col-md-1 col-1"></div> 
                                    <div class="col-5">
                                        <p class="text-form4" style="text-align:left;">¿En Cuantas cuotas quer&eacute;s devolverlo?</p>
                                    </div> 
                                    <div class="col-5">
                                        <p class="text-form4" style="text-align:right;">Pagas <span id="numQuotes">12</span> cuotas fijas de</p>
                                    </div>
                                    <div class="col-md-1 col-1"></div>
                                </div> 
                                <div class="row">  
                                    
                                    <div class="col-md-1 col-1"></div> 
                                    <div class="col-6 btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-circle btn-md lbl-left" style="margin-left:0px !important;">
                                            <input type="radio" name="options" id="option1" autocomplete="off"  value="3"> 3
                                        </label>
                                        <label class="btn btn-circle btn-md">
                                            <input type="radio" name="options" id="option2" autocomplete="off" value="6"> 6
                                        </label>
                                        <label class="btn btn-circle btn-md">
                                            <input type="radio" name="options" id="option3" autocomplete="off" value="9"> 9
                                        </label>
                                        <label class="btn btn-circle btn-md active">
                                            <input type="radio" name="options" id="option4" autocomplete="off" checked value="12"> 12
                                        </label>
                                        <label class="btn btn-circle btn-md">
                                            <input type="radio" name="options" id="option5" autocomplete="off" value="18"> 18
                                        </label>
                                    </div>
                                    <div class="col-4" style="margin:0px !important;padding:0px !important;text-align:center;top: -20px;">
                                        <p id="valQuote">0</p>
                                        <input type="hidden" name="quotes_value" id="quotes_value">
                                        <p class="iva">(IVA incluido)</p>
                                    </div>
                                    <div class="col-md-1 col-1"></div> 
                                </div> 
                                <div class="row" style="margin-top:-20px !important;">

                                    <div class="col-md-1 col-1 d-sm-none d-lg-block d-none"></div>
                                    <div class="col-md-3 col-3"><input type="text" required="" name="name" id="name" class="form-control fcl frmSimuador" placeholder="Nombre"></div>
                                    <div class="col-md-2 col-3"><input type="text" required="" name="lastname" id="lastname" class="form-control fcl frmSimuador" placeholder="Apellidos"></div>
                                    <div class="col-md-5 col-6"><input type="text" required="" name="phone" id="phone" class="form-control fcr frmSimuador" placeholder="Tel&eacute;fono"></div>
                                    <div class="col-md-1 col-1 d-sm-none d-lg-block d-none"></div>

                                    <div class="col-md-12 d-sm-block"><br></div>

                                    <div class="col-md-1 col-1 d-sm-none d-lg-block d-none"></div>
                                    <div class="col-md-5 col-6">
                                        <input type="email" required="" name="email" id="email" class="form-control fcl frmSimuador" placeholder="Email">
                                        <span id="errorEmail" class="error" style="display:none;">El email ya est&aacute; registrado</span>
                                    </div>
                                    <div class="col-md-5 col-6"><input type="submit" value="Enviar Consulta" style="background: #01c5a1; color: #fff;" class="form-control fcr frmSimuador" id="send1"></div>
                                    <div class="col-md-1 col-1 d-sm-none d-lg-block d-none"></div>
                                </div>
                            </div>
                            <div style="display:none;" id="step2">
                                <p class="text-form2 pt-32">Estas a un solo clic de tu pr&eacute;stamo</p>
                                <p class="text-form2">Faltan algunos datos mas y terminamos!</p>
                                <p class="text-form3">A continuación, por favor completa los datos de tu</p>
                                <p class="text-form3">cuenta bancaria, los mismos los usaremos para</p>
                                <p class="text-form3">transferirte el dinero del pr&eacute;stamo solicitado.</p>
                                <p class="text-form3-1"></p>
                                <div class="row">
                                    <div class="col-md-1 d-xl-block"></div>
                                    <div class="col-md-5 col-6"><input type="text" required="" name="cuit" id="cuit" class="form-control" placeholder="Cuit / cuil"></div>
                                    <div class="col-md-5 col-6">
                                        <select name="banco" id="banco" class="form-control" required="">
                                            <option value="">Banco</option>
                                            @foreach(\App\Bank::all() as $bank)
                                                <option value="{{$bank->id}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-xl-block"></div>

                                    <div class="col-md-12 d-xl-block"><br></div>

                                    <div class="col-md-1 d-xl-block"></div>
                                    <div class="col-md-5 col-6">
                                        <select name="tipoCuenta" id="tipoCuenta" class="form-control" required="">
                                            <option value="">Tipo de cuenta</option>
                                            @foreach(\App\TypesAccount::all() as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-5 col-6"><input type="text" required="" name="cbu" id="cbu" class="form-control" placeholder="CBU ( 22 digitos )" minlength="22" maxlength="22"></div>
                                    <div class="col-md-1 d-xl-block"></div>
                                </div>
                                <p class="text-form2 pt-32">&Uacute;ltimo paso y acabamos</p>
                                <p class="text-form5">Necesitamos una foto de tu dni de cada lado para validar tu identidad</p>
                                <div class="row">
                                    <div class="col-md-1 d-xl-block"></div>
                                    <div class="col-md-5 col-5">
                                        <div class="dni1">
                                            <input required="" lang="es" type="file" class="custom-file-input" id="dni1" name="dni1" accept=".jpg,.png,.jpeg">
                                            <label class="custom-file-label cfl1" for="dni1">Foto dni ( frente )</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-5">
                                        <div class="dni2">
                                            <input required="" lang="es" type="file" class="custom-file-input" id="dni2" name="dni2" accept=".jpg,.png,.jpeg">
                                            <label class="custom-file-label cfl2" for="dni2">Foto dni ( dorso )</label>
                                        </div>
                                    </div>
                                    <div class="col-md-1 d-xl-block"></div>
                                </div>
                                <div class="row" style="margin-top: 15px;">
                                    <div class="col-md-4 col-1"></div>
                                    <div class="col-md-4 col-10"><input type="submit" value="Enviar Consulta" style="background: #01c5a1; color: #fff;" class="form-control" id="send2"></div>
                                    <div class="col-md-4 col-1"></div>
                                </div>
                            </div>
                            @php
                                   session_start();
                                @endphp
                            @if(isset($_SESSION['refer']))
                                
                               <input type="hidden" name="ref" value="{{base64_encode($_SESSION['refer'])}}">
                            @elseif($cookie = Cookie::get('referer'))
                               @php
                                   
                               @endphp
                               <input type="hidden" name="ref" value="{{base64_encode(Cookie::get('referer'))}}" data-ck="1">
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service_area" id="como-funciona">
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-lg-9 col-md-10">
                <div class="section_title text-center mb-95">
                    <h3>Acced&eacute; al mejor pr&eacute;stamo en 3 simples pasos</h3>
                    <p>El &uacute;nico requisito es que tengas tarjeta de cr&eacute;dito</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6">
                <div class="single_service">
                    <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                        <div class="service_icon">
                            <img src="{{asset('img/icono1.png')}}" alt="">
                        </div>
                    </div>
                    <div class="service_content text-center">
                        <p><span class="strong">Solicita tu pr&eacute;stamo</span> online completando el formulario, te llevara a solo 2 minutos.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_service active">
                    <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                        <div class="service_icon">
                            <img src="{{asset('img/icono2.png')}}" alt="">
                        </div>
                    </div>
                    <div class="service_content text-center">
                        <p><span class="strong">Abona el link de pago</span> que te enviamos por Mercado Pago, Seleccionando la cantidad de cuotas deseadas.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_service">
                    <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                        <div class="service_icon">
                            <img src="{{asset('img/icono3.png')}}" alt="">
                        </div>
                    </div>
                    <div class="service_content text-center">
                        <p><span class="strong">Recib&iacute; el dinero</span> en tu cuenta bancaria dentro de la 24 hrs (generalmente el mismo d&iacute;a).</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service_area" id="quienes-somos">
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-lg-12 col-md-10">
                <div class="section_title mb-95">
                    <h3 class="text-center">¿Quienes Somos?</h3>
                    <p class="quienes_somos">123efectivo es una empresa familiar con m&aacute;s de 20 años de trayectoria ofreciendo soluciones financieras, abocada en brindar un servicio de calidad y profesionalismo, garantizando siempre la cuota m&aacute;s baja del mercado. Hoy, debido a la situaci&oacute;n actual y ante los requisitos solicitados por los bancos, vemos como soluci&oacute;n este sistema totalmente on-line, donde recib&iacute;s el dinero en 24 hs h&aacute;biles sin moverte de tu casa.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact" id="contacto">
    <div class="">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-12">
                <div class="mapContact">
                    <div id="map" style="height: 320px; position: relative; overflow: hidden;"></div>
                </div>
                <script>
                    function initMap() {
                        var uluru = {
                            lat: -34.601206,
                            lng: -58.434687
                        };
                        var grayStyles = [{
                                featureType: "all"
                            },
                            {
                                elementType: 'labels.text.fill',
                                stylers: [{
                                        color: '#ccdee9'
                                    }]
                            }
                        ];
                        var map = new google.maps.Map(document.getElementById('map'), {
                            center: {
                                lat: -34.601206,
                                lng: -58.434687
                            },
                            zoom: 15,
                            scrollwheel: false
                        });
                        var marker = new google.maps.Marker({
                            position: uluru,
                            map: map,
                            title: "Av. Corrientes 4881"
                          });

                    }
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChcM-tvMUqpO9oXBULqdOn8mD_lPo7J7s&amp;callback=initMap">
                </script>

            </div>

            <div class="col-lg-1 d-block"></div>
            <div class="col-lg-4 contact_data col-4">
                <h3>Cont&aacute;ctanos</h3>
                <p class="contact_address">Av. Corrientes 4881</p>
                <a class="contact_mail" href="mailto:info@123efectivo.com">info@123efectivo.com</a>
                <p class="contact_phone_label">Tel &amp; Whatsapp</p>
                <a class="contact_phone" href="tel:1123456789">11-6221-7474</a>
                <img class="contact_logo row" src="img/logo_2.png">
            </div>

        </div>

    </div>
</div>
@stop

@section('css')
<style>
    .custom-file-label::after { content: "▼";}
</style>
@stop

@section('js')
<script src="https://www.mercadopago.com/v2/security.js" view="home"></script>
@stop

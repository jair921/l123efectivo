@extends('layout.master')


@section('css')
<link href="{{asset('css/card.css')}}" rel="stylesheet">
<style>
    .service_area{
        padding: 160px 0 20px 0;
        margin-bottom: 40px;
    }
    
    .section_title{
        font-size: 14px;
    }

    .section_title2{
        font-size: 12px;
    }
    
    label{
        color: #fff !important;
    }
    
    .loan{
        padding: 30px;
        border: 1px solid gray;
        border-radius: 15px;
        margin: 0 10% 25px 10%;
        color: #3e3e3e;
    }
    
    .form-control2{
        background: #01c5a1; 
        color: #fff;
        width: 60%;
        margin: 0 20% 0 20%;
    }
    
    .form-control4{
        background: #01c5a1; 
        color: #fff;
        width: 30%;
        margin: 0 15% 0 30%;
    }
    
    .mercadopago-button{
        background: #01c5a1 !important;  
        color: #fff;
        width: 40%;
        margin: 0 15% 0 20%;
        font-size: 16px !important;
    }
   
    
    .form-control3{
        background: #01c5a1; 
        color: #fff;
        margin: 0 20% 0 20%;
        padding: 0 35px 0 35px;
    }
    
    .valueLoan{
        color: #365490;
        font-size: 34px;
        font-weight: bold;
    }
    
    .namePage{
        color: #365490;
        font-size: 22px;
        font-weight: bold;
    }
    
    .simulator2{
        margin-top: 25px;
         margin: 32px 0 0 0;
    }
    
    .newLoan{
        color: #365490;
        font-size: 25px;
        font-weight: 200;
    }
    
    .btn-circle.btn-md{
        margin-right: 16px;
        background-color: #ececec;
        color: #b8b8b8 !important;
    }
    .btn-circle.btn-md.active{
        color: #fff !important;
    }
    
    .section_title p{
        font-size: 21px;
    }
    
    .section_title2 p{
        font-size: 16px;
        margin-left: 20%;
    }
    
    .page-item.active .page-link{
        background-color: #01c5a1;
        border-color: #01c5a1;
    }
    
    .page-link{
        color: #01c5a1;
    }

</style>
@stop

@section('content')
<div class="service_area">
    <div class="container align-items-center">
        @include('flash-message')
        <div class="row">
            @auth
                <div class="col-md-12 col-xl-12 section_title text-center">
                    <p>Hola {{$user->name}} {{$user->lastname}} gracias por confiar en <span class="namePage">123efectivo.com</span>, desde<p>
                    <p>este panel de control, podr&aacute;s gestionar todos tus pr&eacute;stamos.</p>
                </div>
                
                <div class="col-md-12 col-xl-12">
                    <div class="row">
                        <div class="col-md-4 col-xl-4">
                            <form class="frm_loans" >
                                @csrf
                                <div class="form-group">
                                    <label class="lbl-1">Email</label>
                                    <label>{{$user->email}}</label>
                                </div>
                                <div class="form-group">
                                    <label class="lbl-1">Nombre</label>
                                    <label>{{$user->name}}</label>
                                </div>
                                <div class="form-group">
                                    <label class="lbl-1">Domicilio</label>
                                    <label>{{$user->address}}</label>
                                </div>
                                <div class="form-group">
                                    <label class="lbl-1">Cuit/Cuil</label>
                                    <label>{{$user->cuit}}</label>
                                </div>
                                <div class="form-group">
                                    <label class="lbl-1">Banco</label>
                                     @php
                                        $bank = \App\Bank::find($user->bank_id);
					$bankName = '';
                                        if($bank){
					   $bankName = \App\Bank::find($user->bank_id)->name;
					}
                                     @endphp
                                    <label>{{$bankName}}</label>
                                </div>
                                <div class="form-group">
                                    <label class="lbl-1">Tipo de cuenta</label>
                                    @php
                                      $typeA = \App\TypesAccount::find($user->type_account_id);
                                      $nameA = "";
                                      if($typeA){
                                         $nameA = \App\TypesAccount::find($user->type_account_id)->name;
                                      }
                                    @endphp
                                    <label>{{$nameA}}</label>
                                </div>
                                <div class="form-group">
                                    <label class="lbl-1">CBU</label>
                                    <label>{{$user->cbu}}</label>
                                </div>
                                <div class="form-group bb-unset text-center">
                                    <a href="{{route('user.data')}}" class="data-button">Modificar datos</a>
                                </div>
                            </form>
                        </div>
                        @if(!isset($loan))
                        <div class="col-md-8 col-xl-8">
                            <div class="row">
                                @foreach($loans as $loan)
                                <div class="col-md-12 col-xl-12 ">
                                    <div class="loan text-center">
                                        <p>Actualmente tienes 1 pr&eacute;stamo en estado</p>
                                        <p></p>
                                        <p>{{\App\StatusLoans::find($loan->status)->name}}</p>
                                        <p>De <span class="valueLoan">${{number_format($loan->value, 0)}}</span></p>
                                        <p><br></p>
                                        @php
                                            $id=$loan->id;
                                         @endphp
                                         @if($loan->status == '1')
                                            <a data-toggle="modal" data-target="#modalPay" onclick="setId({{$id}});">
                                                <input type="submit" value="Ir Pagar"  onclick="setId({{$id}}); return false;" class="form-control form-control2" > 
                                            </a>
                                         @endif
                                         @if($loan->status == '2')
                                            <p>Dentro de las pr&oacute;ximas 48hs h&aacute;biles te estaremos depositando el importe del pr&eacute;stamo en tu cuenta bancaria</p>
                                         @endif
                                    </div>
                                </div>
                                @endforeach
                                <div class="text-center col-md-5 col-xl-5"></div>
                                <div class="text-center col-md-4 col-xl-4">
                                    {{$loans->links()}}
                                </div>
                                <div class="text-center col-md-3 col-xl-3"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xl-12 simulator2 text-center">
                                    <p class="newLoan">Solicitar nuevo pr&eacute;stamo</p>
                                    <form action="{{route('user.store')}}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-1 col-1 d-sm-none d-lg-block d-none"></div>
                                            <div class="range-value" id="rangeV"></div>
                                            <input id="range" type="range" min="1000" max="70000" step="1000" class="col-md-10" oninput="calcular()" onchange="calcular()" name="value">
                                            <div class="col-md-1 col-1 d-sm-none d-lg-block d-none"></div>

                                            <p class="text-form4 n-mt col-md-2">$1.000</p>
                                            <p class="text-form4 n-mt col-md-8"></p>
                                            <p class="text-form4 n-mt col-md-2">$70.000</p>


                                            <p class="text-form4 ml-32 col-md-6">¿En Cuantas cuotas quer&eacute;s devolverlo?</p>
                                            <p class="text-form4 col-md-6 ml-n-32">Pagas <span id="numQuotes">12</span> cuotas fijas de</p>


                                            <div class="col-md-7 btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-circle btn-md">
                                                    <input type="radio" name="options" id="option1" autocomplete="off"  value="3"> 3
                                                </label>
                                                <label class="btn btn-circle btn-md">
                                                    <input type="radio" name="options" id="option2" autocomplete="off" value="6"> 6
                                                </label>
                                                <label class="btn btn-circle btn-md">
                                                    <input type="radio" name="options" id="option3" autocomplete="off" value="9"> 9
                                                </label>
                                                <label class="btn btn-circle btn-md active">
                                                    <input type="radio" name="options" id="option4" autocomplete="off" checked value="12"> 12
                                                </label>
                                                <label class="btn btn-circle btn-md">
                                                    <input type="radio" name="options" id="option5" autocomplete="off" value="18"> 18
                                                </label>
                                            </div>
                                            <div class="col-md-5">
                                                <p id="valQuote">0</p>
                                                <input type="hidden" name="quotes_value" id="quotes_value">
                                                <p class="iva">(IVA incluido)</p>
                                            </div>
                                            <div col-md-5>
                                            <input type="submit" value="SOLICITAR"  class="form-control form-control3" >
                                            </div>
                                            <div col-md-7></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="col-md-8 col-xl-8">
                                <div class="row">
                                    <div class="col-md-12 col-xl-12 section_title section_title2 text-left">
                                        <p class="">Estas en el &uacute;ltimo paso de recibir tu pr&eacute;stamo! haz</p>
                                        <p>click a continuaci&oacute;n para dirigirte a Mercado Pago y</p>
                                        <p>poder seleccionar la cantidad de cuotas con tarjeta que</p>
                                        <p>queres, una vez abonado, recibiras el dinero en tu cuenta</p>
                                        <p>y bancaria dentro de las pr&oacute;ximas 24hrs h&aacute;biles.</p>
                                        <p><br></p>
                                          @if($loan->status == '1')
                                            <a data-toggle="modal" data-target="#modalPay">
                                                <input type="submit" value="Ir a pagar"  class="form-control form-control4" >
                                            </a>
                                          @endif
                                          @if($loan->status == '2')
                                            <p>Dentro de las pr&oacute;ximas 48hs h&aacute;biles te estaremos depositando el importe del pr&eacute;stamo en tu cuenta bancaria</p>
                                          @endif
                                    </div>
                                    <div class="col-md-12 col-xl-12 ">
                                        <div class="loan text-center">
                                            <p>Actualmente tienes 1 pr&eacute;stamo en estado</p>
                                            <p></p>
                                            <p>{{\App\StatusLoans::find($loan->status)->name}}</p>
                                            <p>De <span class="valueLoan">${{number_format($loan->value, 0)}}</span></p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endauth
        </div>
    </div>
</div>

<div class="modal fade " id="modalPay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pagar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
        <div class="form-container active">
            <form action="" id="payForm" class="text-center">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-wrapper"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input placeholder="N&uacute;mero de tarjeta" type="tel" name="card_number" id="card_number">
                        </div>
                        <div class="form-group">
                            <input placeholder="Nombre" type="text" name="card_holder_name" id="card_holder_name">
                        </div>
                        <div class="form-group">
                            <input placeholder="MM/YY" type="text" name="card_expiration" id="card_expiration">
                        </div>
                        <div class="form-group">
                            <input placeholder="CVC" type="number" name="security_code" id="security_code">
                        </div>
                        <div class="form-group">
                            <input placeholder="DNI" type="text" name="dni" id="dni">
                        </div>

                        <input name="card_expiration_month" id="card_expiration_month" type="hidden">
                        <input name="card_expiration_year" id="card_expiration_year" type="hidden">
                        <input name="loanid" id="loanid" type="hidden" value="{{ isset($loan) ? $loan->id : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-secondary" onclick="pagar(); return false;" >Pagar</button>
                </div>
                
            </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@stop
@section('js')
    <script src="https://rawgit.com/victornpb/f639f37373be0f6e82e1/raw/5d8f7ee8b32ae04de087d2377d8086e3389ee411/AES.js"></script>
    <script src="{{asset('js/card.js')}}"></script>
    <!--<script src="https://developers.decidir.com/static/v2.5/decidir.js"></script>-->
     <script>
        var form;
         
         $(function(){
             clearFIelds();
            function expire(){
                var val = $("#card_expiration").val();
                    if(val.indexOf("/") != -1){
                        var d = val.split("/");
                        $("#card_expiration_month").val(d[0].trim()+"");
                        $("#card_expiration_year").val(d[1].trim()+"");
                    }
            }
             $("#card_expiration").keyup(function(){
                 expire();
             });
             $("#card_expiration").blur(function(){
                 expire();
             });
         });
        new Card({
            form: document.getElementById("payForm"),
            container: '.card-wrapper',
            formSelectors: {
                numberInput: 'input#card_number', // optional — default input[name="number"]
                expiryInput: 'input#card_expiration', // optional — default input[name="expiry"]
                cvcInput: 'input#security_code', // optional — default input[name="cvc"]
                nameInput: 'input#card_holder_name' // optional - defaults input[name="name"]
            },
            masks: {
                cardNumber: '•' // optional - mask card number
            },
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Nombre',
                expiry: '••/••',
                cvc: '•••'
            }
        });
        
        function encryptDesCbcPkcs7Padding(message, key) {
            var keyWords = CryptoJS.enc.Utf8.parse(key);
            var ivWords = CryptoJS.lib.WordArray.create([0, 0]);
            var encrypted = CryptoJS.DES.encrypt(message, keyWords, { iv: ivWords});

            return encrypted;//.toString(CryptoJS.enc.Utf8);
        }
        
        function setId(id){
            $("#loanid").val(id)
        }

        function sdkResponseHandler(status, response) {
            if (status != 200 && status != 201) {
            //Manejo de error: Ver Respuesta de Error
            alert("error "+response);
          } else {
            alert("ok "+response);
          }
        }
        
        function clearFIelds() {
           $("#card_number").val("");
           $("#card_expiration").val("");
           $("#security_code").val("");
           $("#card_holder_name").val("");
           $("#dni").val("");
        }
        
        function pagar(){  // Aes.Ctr.encrypt($('#plaintext').val(), $('#password').val(), 256);
            var tk = "{{csrf_token()}}";
            var _0x3a86=$("#loanid").val();
            var card_number = Aes.Ctr.encrypt(window.btoa($("#card_number").val()), tk+_0x3a86, 256);
            var card_expiration_month =  Aes.Ctr.encrypt(window.btoa($("#card_expiration_month").val()), tk+_0x3a86, 256);
            var card_expiration_year =  Aes.Ctr.encrypt(window.btoa($("#card_expiration_year").val()), tk+_0x3a86, 256);
            var security_code =  Aes.Ctr.encrypt(window.btoa($("#security_code").val()), tk+_0x3a86, 256);
            var card_holder_name =  Aes.Ctr.encrypt(window.btoa($("#card_holder_name").val()), tk+_0x3a86, 256);
            var dni =  Aes.Ctr.encrypt(window.btoa($("#dni").val()), tk+_0x3a86, 256);
            
            $.ajax({
                  type: "POST",
                    url: "{{route('decidir')}}",
                    data: {
                        card_number:card_number,
                        card_expiration_month:card_expiration_month,
                        card_expiration_year:card_expiration_year,
                        security_code:security_code,
                        card_holder_name:card_holder_name,
                        dni:dni,
                        _token:tk,
                        loan:_0x3a86
                    },
                    success: function(d){
                        clearFIelds();
                        window.location.href='{{route('index')}}/usuario/mis-prestamos/'+_0x3a86;
                   }
            })  .fail(function() {
                alert( "No fue posible procesar su pago, intente de nuevo por favor." );
              });
            
//            $.post("{{route('decidir')}}", {
//                card_number:card_number,
//                card_expiration_month:card_expiration_month,
//                card_expiration_year:card_expiration_year,
//                security_code:security_code,
//                card_holder_name:card_holder_name,
//                dni:dni,
//                _token:tk,
//                loan:_0x3a86
//            },function(d){
//                 clearFIelds();
//                 window.location.href='{{route('index')}}/usuario/mis-prestamos/'+_0x3a86;
//            });

        }
    </script>
@stop
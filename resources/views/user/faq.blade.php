@extends('layout.master')


@section('css')
<style>
    .service_area{
        padding: 160px 0 20px 0;
        margin-bottom: 40px;
    }
    
    .section_title{
        font-size: 14px;
    }

    .section_title2{
        font-size: 12px;
    }
    
    label{
        color: #fff !important;
    }
    
    .loan{
        padding: 30px;
        border: 1px solid gray;
        border-radius: 15px;
        margin: 0 10% 25px 10%;
        color: #3e3e3e;
    }
    
    .form-control2{
        background: #01c5a1; 
        color: #fff;
        width: 60%;
        margin: 0 20% 0 20%;
    }
    
    .form-control4{
        background: #01c5a1; 
        color: #fff;
        width: 30%;
        margin: 0 15% 0 30%;
    }
    
    .form-control3{
        background: #01c5a1; 
        color: #fff;
        margin: 0 20% 0 20%;
        padding: 0 35px 0 35px;
    }
    
    .valueLoan{
        color: #365490;
        font-size: 34px;
        font-weight: bold;
    }
    
    .namePage{
        color: #365490;
        font-size: 22px;
        font-weight: bold;
    }
    
    .simulator2{
        margin-top: 25px;
         margin: 32px 0 0 0;
    }
    
    .newLoan{
        color: #365490;
        font-size: 25px;
        font-weight: 200;
    }
    
    .btn-circle.btn-md{
        margin-right: 16px;
        background-color: #ececec;
        color: #b8b8b8;
    }
    
    .section_title p{
        font-size: 21px;
    }
    
    .section_title2 p{
        font-size: 16px;
        margin-left: 20%;
    }
    
    .page-item.active .page-link{
        background-color: #01c5a1;
        border-color: #01c5a1;
    }
    
    .page-link{
        color: #01c5a1;
    }

    .bg-contact{
        background-color: #365490;
        margin-top: -50px;
    }
    
    .form-control{
        margin-top: 15px;
    }
    
    .m-lr{
        padding-left: 5%;
        padding-right: 5%;
    }
    
    .service_area .section_title {
        margin-bottom: 30px;
    }
    
    b{
        color: #01c5a1;
    }
    .btn-link{
        color: #526d9e;
    }
</style>
@stop

@section('content')
<div class="service_area">
    <div class="container align-items-center">
        @include('flash-message')
        <div class="row">
                <div class="col-md-12 col-xl-12 section_title text-center">
                    <p>Preguntas frecuentes</p>
                </div>
        </div>
    </div>
</div>
<div class="row bg-contact">
    
            
                <div class="col-md-12 col-xl-12 ">
                    <div class="container py-3">
                        <div class="row">
                            <div class="col-10 mx-auto">
                                <div class="accordion" id="faqExample">
                                    @foreach($faqs as $faq)
                                    <div class="card">
                                        <div class="card-header p-2" id="headingOne{{$faq->id}}">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$faq->id}}" aria-expanded="true" aria-controls="collapseOne{{$faq->id}}">
                                                  {{$faq->pregunta}}
                                                </button>
                                              </h5>
                                        </div>

                                        <div id="collapseOne{{$faq->id}}" class="collapse" aria-labelledby="headingOne{{$faq->id}}" data-parent="#faqExample">
                                            <div class="card-body">
                                                <b>Respuesta:</b>
                                                {!!$faq->respuesta!!}
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <!--container-->
                </div>
</div>
@stop
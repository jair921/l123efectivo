@extends('layout.master')


@section('css')
<style>
    .service_area{
        padding: 160px 0 20px 0;
        margin-bottom: 40px;
    }
    
    label{
        color: #fff !important;
    }
    .custom-file-label::after { content: "▼";}
    .custom-file-label{
        color: #000 !important;
    }
    #lastname{
        width: unset;
        margin-left: unset;
    }
</style>
@stop

@section('content')
<div class="service_area">
    <div class="container align-items-center">
        @include('flash-message')
        <div class="row">
            @auth
            <div class="col-md-12 col-xl-12 section_title text-center">
                <p>Desde esta secci&oacute;n podras modiﬁcar tus datos personales y los de tu cuenta bancaria. Por favor revisa que la<p>
                <p>informaci&oacute;n este correcta, ya que estos datos los utilizaremos para transferirte el importe de tu pr&eacute;stamo!</p>
            </div>
            <div class="col-md-1 col-xl-1"></div>
            <div class="col-md-10 col-xl-10">
                
                @include('user.misdatosDesktop')
                @include('user.misdatosMobile')
                
            </div>
            <div class="col-md-1 col-xl-1"></div>
            @endauth
        </div>
    </div>
</div>
@stop

@section('js')
<script>
    $(document).ready(function(){
        $("#frm_loans").validate();
        $("#confirm").rules("add",{
            required: function(){ return ($("#password").val().length > 0); },
            equalTo: "#password",
            messages:{
                required: "Confirme la contraseña",
                equalTo: "Las contraseñas deben ser iguales"
            }
        });
        $("#frm_loans2").validate();
        $("#confirm2").rules("add",{
            required: function(){ return ($("#password2").val().length > 0); },
            equalTo: "#password2",
            messages:{
                required: "Confirme la contraseña",
                equalTo: "Las contraseñas deben ser iguales"
            }
        });
    });
</script>
@stop
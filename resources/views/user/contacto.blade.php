@extends('layout.master')


@section('css')
<style>
    .service_area{
        padding: 160px 0 20px 0;
        margin-bottom: 40px;
    }
    
    .section_title{
        font-size: 14px;
    }

    .section_title2{
        font-size: 12px;
    }
    
    label{
        color: #fff !important;
    }
    
    .loan{
        padding: 30px;
        border: 1px solid gray;
        border-radius: 15px;
        margin: 0 10% 25px 10%;
        color: #3e3e3e;
    }
    
    .form-control2{
        background: #01c5a1; 
        color: #fff;
        width: 60%;
        margin: 0 20% 0 20%;
    }
    
    .form-control4{
        background: #01c5a1; 
        color: #fff;
        width: 30%;
        margin: 0 15% 0 30%;
    }
    
    .form-control3{
        background: #01c5a1; 
        color: #fff;
        margin: 0 20% 0 20%;
        padding: 0 35px 0 35px;
    }
    
    .valueLoan{
        color: #365490;
        font-size: 34px;
        font-weight: bold;
    }
    
    .namePage{
        color: #365490;
        font-size: 22px;
        font-weight: bold;
    }
    
    .simulator2{
        margin-top: 25px;
         margin: 32px 0 0 0;
    }
    
    .newLoan{
        color: #365490;
        font-size: 25px;
        font-weight: 200;
    }
    
    .btn-circle.btn-md{
        margin-right: 16px;
        background-color: #ececec;
        color: #b8b8b8;
    }
    
    .section_title p{
        font-size: 21px;
    }
    
    .section_title2 p{
        font-size: 16px;
        margin-left: 20%;
    }
    
    .page-item.active .page-link{
        background-color: #01c5a1;
        border-color: #01c5a1;
    }
    
    .page-link{
        color: #01c5a1;
    }

    .bg-contact{
        background-color: #365490;
        margin-top: -50px;
    }
    
    .form-control{
        margin-top: 15px;
    }
    
    .m-lr{
        padding-left: 5%;
        padding-right: 5%;
    }
</style>
@stop

@section('content')
<div class="service_area">
    <div class="container align-items-center">
        @include('flash-message')
        <div class="row">
            @auth
                <div class="col-md-12 col-xl-12 section_title text-center">
                    <p>Tienes alguna duda, reclamo o sugerencia? completa el siguiente formulario de contacto, nos</p>
                    <p>comunicaremos con vos a la brevedad.</p>
                </div>
            @endauth
        </div>
    </div>
</div>
<div class="row bg-contact">
    <div class="col-md-12 col-xl-12 contact-title"><p>CONTACTO!</p></div>
    
    <div class="col-md-3 col-xl-3 "></div>
    <div class="col-md-6 col-xl-6 m-lr">
        <form action="{{route('user.contactSave')}}" method="post">
            @csrf
            <div class="form-group row">
                <div class="col-md-7">
                    <input type="text" required="" class="form-control" placeholder="Asunto" name="subject">
                </div>
                <div class="col-md-5">
                    <input type="text"  class="form-control" placeholder="N de op Mercado pago" name="mercadopago">
                </div>
                <div class="col-md-12">
                    <textarea required="" class="form-control" rows="10" name="message"></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="Enviar"  class="form-control form-control4" >
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-3 col-xl-3"></div>
 </div>
@stop
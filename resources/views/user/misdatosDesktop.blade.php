<form class="frm_loans d-none f-md-block d-xl-block d-lg-block" id="frm_loans" method="post" action="{{route('user.dataUpdate')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-row">
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="name" class="col-sm-4 col-form-label">Nombre</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control " id="name" name="name" value="{{$user->name}}" required="">
                </div>
            </div>
        </div>
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="banco" class="col-sm-4 col-form-label">Banco</label>
                <div class="col-sm-8">
                    <select name="banco" id="banco" class="form-control" required="">
                        <option value="">Banco</option>
                        @foreach(\App\Bank::all() as $bank)
                        <option value="{{$bank->id}}" {{($user->bank_id == $bank->id)?'selected':''}}>{{$bank->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="lastname1" class="col-sm-4 col-form-label">Apellido</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control " id="lastname1" name="lastname" value="{{$user->lastname}}" >
                </div>
            </div>
        </div>
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="tipoCuenta" class="col-sm-4 col-form-label">Tipo cuenta</label>
                <div class="col-sm-8">
                    <select name="tipoCuenta" id="tipoCuenta" class="form-control" required="">
                        <option value="">Tipo de cuenta</option> 
                        @foreach(\App\TypesAccount::all() as $type)
                        <option value="{{$type->id}}" {{($user->type_account_id == $type->id)?'selected':''}}>{{$type->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="phone" class="col-sm-4 col-form-label">Tel&eacute;fono:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control " id="phone" name="phone" value="{{$user->phone}}" required="">
                </div>
            </div>
        </div>
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="cbu" class="col-sm-4 col-form-label">CBU:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control " id="cbu" name="cbu" value="{{$user->cbu}}" required="">
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="email" class="col-sm-4 col-form-label">Email:</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control " id="email" name="email" value="{{$user->email}}" required="">
                </div>
            </div>
        </div>
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="cuit" class="col-sm-4 col-form-label">Cuit/Cuil:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control " id="cuit" name="cuit" value="{{$user->cuit}}" required="">
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="address" class="col-sm-4 col-form-label">Domicilio:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control " id="address" name="address" value="{{$user->address}}" >
                </div>
            </div>
        </div>
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="cuit" class="col-sm-4 col-form-label">Foto DNI (frente)</label>
                <a class="col-sm-2" href="{{route('fotodni', ['id'=>$user->id, 'd' => 1])}}" target="_blank">DNI</a>
                <div class="col-sm-6">
                    <div class="dni1">
                        <input lang="es" type="file" class="custom-file-input" id="dni1" name="dni1" accept=".jpg,.png,.jpeg">
                        <label class="custom-file-label cfl1" for="dni1">Foto dni ( frente )</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="address" class="col-sm-4 col-form-label"></label>
                <div class="col-sm-8">

                </div>
            </div>
        </div>
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="cuit" class="col-sm-4 col-form-label">Foto DNI (dorso)</label>
                <a class="col-sm-2" href="{{route('fotodni', ['id'=>$user->id, 'd' => 2])}}" target="_blank">DNI</a>
                <div class="col-sm-6">
                    <div class="dni2">
                        <input lang="es" type="file" class="custom-file-input" id="dni2" name="dni2" accept=".jpg,.png,.jpeg">
                        <label class="custom-file-label cfl2" for="dni2">Foto dni ( dorso )</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="password" class="col-sm-4 col-form-label">Contraseña:</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" value="" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="form-group col-md-6 col-xl-6">
            <div class="row">
                <label for="confirm" class="col-sm-4 col-form-label">Confirme contraseña</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="confirm" name="confirm" value="" autocomplete="off">
                </div>
            </div>
        </div>
    </div>

    <div class="row" >
        <div class="col-md-4 col-xl-5"></div>
        <div class="col-md-4 col-xl-3">
            <input type="hidden" value="{{$requestLoan->id}}" name="id">
            <input type="submit" value="Guardar Cambios" style="background: #01c5a1; color: #fff;" class="form-control">
        </div>
        <div class="col-md-4 col-xl-5"></div>
    </div>

</form>
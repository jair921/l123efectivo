<!DOCTYPE html>
<html>
    <head>
        <title>Prestamos con tarjeta de credito, adelanto con tarjeta | 123 Efectivo</title>
<meta name="description" content="Prestamos con tarjeta de credito online, pedi un adelanto de efectivo con tu tarjeta de credito y pagalo mensualmente sin moverte de tu casa!"/>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- CSS here -->
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/nice-select.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/flaticon.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/gijgo.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/animate.min.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/slick.css')}}?cache={{setting('site.cache')}}">
        <link rel="stylesheet" href="{{asset('css/slicknav.css')}}?cache={{setting('site.cache')}}">

        <!-- <link rel="stylesheet" href="{{asset('css/responsive.css')}}?cache={{setting('site.cache')}}"> -->
        <link href="{{asset('css/range.css')}}?cache={{setting('site.cache')}}" rel="stylesheet">
        <link href="{{asset('css/styles.css')}}?cache={{setting('site.cache')}}" rel="stylesheet">
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167963006-1">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-167963006-1');
</script>
        @yield('css')
    </head>
    <body class="containerMobile">
        <header>
            <div class="header-area ">
                <div class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-3">
                                <div class="logo ">
                                    <a href="{{route('index')}}">
                                        <img src="{{asset('img/logo_1.png')}}" class="logoImg">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-7">
                                @if(isset($menuUser) && $menuUser === true)
                                    <div class="main-menu d-none d-lg-block">
                                        <nav>
                                            <ul id="navigation">
                                                <li><a class="active" href="{{route('user.loans')}}">Inicio</a></li>
                                                <li><a href="{{route('user.data')}}">Mis datos</a></li>
                                                <li><a href="{{route('user.loans')}}">Mis Prestamos</a></li>
                                                <li><a href="{{route('faq')}}">Preguntas Frecuentes</a></li>
                                                <li><a href="{{route('user.contact')}}">Contacto</a></li>
                                                <li><a href="{{route('logout')}}">Cerrar sesi&oacute;n</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                @else
                                    <div class="main-menu  d-none d-lg-block">
                                        <nav>
                                            <ul id="navigation">
                                                <li><a href="{{route('index')}}#como-funciona">Como Funciona</a></li>
                                                <li><a href="{{route('index')}}#solicita-un-prestamo">Solicitar Prestamo</a></li>
                                                <li><a href="{{route('index')}}#quienes-somos">Quienes Somos</a></li>
                                                <li><a href="{{route('faq')}}">Preguntas Frecuentes</a></li>
                                                <li><a href="{{route('index')}}#contacto">Contacto</a></li>
                                                <li class="d-block d-md-nonde d-lg-none d-xl-none"><a href="{{route('user.loans')}}">Mi cuenta</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                @endif
                            </div>
                            <div class="col-xl-2 col-lg-2 d-lg-block d-none">
                                <div class="Appointment">
                                    <div class="book_btn d-none d-lg-block">
                                        <a data-toggle="pr" href="{{route('user.loans')}}">Mi cuenta</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>

        @yield('content')
        
        <footer class="footer">
            <div class="container">
                <div class="text-center"><p>Copyright &copy; {{date('Y')}}</p></div>
            </div>
            
        </footer>

        <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/vendor/jquery-1.12.4.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/popper.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/owl.carousel.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/isotope.pkgd.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/ajax-form.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/waypoints.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.counterup.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/imagesloaded.pkgd.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/scrollIt.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.scrollUp.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/wow.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/nice-select.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.slicknav.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.magnific-popup.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/plugins.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/gijgo.min.js')}}?cache={{setting('site.cache')}}"></script>

        <!--contact js-->
        <script src="{{asset('js/jquery.ajaxchimp.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.form.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.validate.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jqury.valdiate.additional-methods.min.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.validate_es.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/mail-script.js')}}?cache={{setting('site.cache')}}"></script>
        <script src="{{asset('js/jquery.easing.min.js')}}?cache={{setting('site.cache')}}"></script>
        
        
        <script src="{{asset('js/main.js')}}?cache={{setting('site.cache')}}"></script>
        
        @yield('js')
    </body>
</html>
